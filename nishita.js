'use strict';

//https://www.scratchapixel.com/lessons/procedural-generation-virtual-worlds/simulating-sky/simulating-colors-of-the-sky

class Atmosphere {
    constructor(
        sunRotation, // -1 to 1 (0 = y direction)
        sunElevation, // -1 to 1 (1 = z direction)
        air,
        dust
    ) {
        this.sunDirection = new v3(
           0,
           1,
           0
        ).rotatedX(sunElevation * Math.PI * .5)
            .rotatedZ(sunRotation * Math.PI)
            .normalized();

        this.earthRadius = 6360000;
        this.atmosphereRadius = 6420000;
        this.airThickness = 7994 * air;
        this.dustThickness = 1200 * dust;
        this.sunPower = 400;
        this.betaR = new v3(.0000038, .0000135, .0000331);
        this.betaM = new v3(.00021);
    }

    getIncidentLight(ray, tMin, tMax) {
        ray.d = ray.d.normalized();
        // we use z === 0 as ocean level so we move the ray up the earth's surface
        ray.o.z += this.earthRadius;

        let atmosphere = new Sphere(this.atmosphereRadius, null, new v3());
        let hit = atmosphere.getHit(ray, tMin, tMax);
        let samples = 2;
        let lightSamples = 2;
        let segmentLength = hit.t / samples;
        let sumR = new v3();
        let sumM = new v3();
        let opticalDepthR = 0
        let opticalDepthM = 0;
        let mu = ray.d.dot(this.sunDirection);
        let phaseR = 3 / (16 * Math.PI) * (1 + mu * mu);
        let g = 0.76;
        let phaseM = 3 / (8. * Math.PI) * ((1. - g * g) * (1. + mu * mu)) / ((2. + g * g) * Math.pow(1. + g * g - 2. * g * mu, 1.5));
        for (let i = 0; i < samples; i++) {
            //let samplePosition = ray.o.vAdded(ray.d.multiplied(i * segmentLength + segmentLength * .5));
            let samplePosition = ray.o.vAdded(ray.d.multiplied(Math.random() * segmentLength));
            let height = samplePosition.size() - this.earthRadius;

            // compute optical depth for light
            let hr = Math.exp(-height / this.airThickness) * segmentLength;
            let hm = Math.exp(-height / this.dustThickness) * segmentLength;
            opticalDepthR += hr;
            opticalDepthM += hm;

            // light optical depth
            hit = atmosphere.getHit(new Ray(samplePosition, this.sunDirection), tMin, tMax);
            if (hit === null) {
                Debug.log('hit === null');
            }
            let segmentLengthLight = hit.t / lightSamples;
            let opticalDepthLightR = 0;
            let opticalDepthLightM = 0;
            let j;
            for (j = 0; j < lightSamples; j++) {
                //let samplePositionLight = samplePosition.vAdded(this.sunDirection.multiplied(j * segmentLengthLight + segmentLengthLight * .5));
                let samplePositionLight = samplePosition.vAdded(this.sunDirection.multiplied(Math.random() * segmentLengthLight));
                let heightLight = samplePositionLight.size() - this.earthRadius;
                if (heightLight < 0) {
                    // in the shadow of the earth
                    break;
                }
                opticalDepthLightR += Math.exp(-heightLight / this.airThickness) * segmentLengthLight;
                opticalDepthLightM += Math.exp(-heightLight / this.dustThickness) * segmentLengthLight;
            }
            if (j === lightSamples) {
                let tau = this.betaR.multiplied(opticalDepthR + opticalDepthLightR)
                    .vAdded(this.betaM.multiplied(1.1 * (opticalDepthM + opticalDepthLightM)));
                let attenuation = new v3(Math.exp(-tau.x), Math.exp(-tau.y), Math.exp(-tau.z))
                    .clamped(0, Number.MAX_VALUE);
                sumR = sumR.vAdded(attenuation.multiplied(hr));
                sumM = sumM.vAdded(attenuation.multiplied(hm));
            }
        }

        let light = sumR.vMultiplied(this.betaR).multiplied(phaseR)
            .vAdded(sumM.vMultiplied(this.betaM).multiplied(phaseM))
            .multiplied(this.sunPower)
            .clamped(0, 10000);

        return light;
    }
}