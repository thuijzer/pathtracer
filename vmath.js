'use strict';

class v3 {
    constructor(x, y, z) {
        if (typeof x === 'object') {
            this.x = x.x;
            this.y = x.y;
            this.z = x.z;
        } else {
            this.x = typeof x !== 'undefined' ? x : .0;
            this.y = typeof y !== 'undefined' ? y : this.x;
            this.z = typeof z !== 'undefined' ? z : this.x;
        }
    }

    normalized() {
        let d = this.size();

        return new v3(
            this.x / d,
            this.y / d,
            this.z / d
        );
    }

    distance(p) {
        return Math.sqrt(
            Math.pow(p.x - this.x, 2)
            + Math.pow(p.y - this.y, 2)
            + Math.pow(p.z - this.z, 2)
        );
    }

    size() {
        return Math.sqrt(this.sizeSq());
    }

    sizeSq() {
        return this.x * this.x
            + this.y * this.y
            + this.z * this.z;
    }

    vSubstracted(v) {
        return new v3(
            this.x - v.x,
            this.y - v.y,
            this.z - v.z
        );
    }

    vAdded(v) {
        return new v3(
            this.x + v.x,
            this.y + v.y,
            this.z + v.z
        );
    }

    added(a) {
        return new v3(
            this.x + a,
            this.y + a,
            this.z + a
        );
    }

    multiplied(m) {
        return new v3(
            this.x * m,
            this.y * m,
            this.z * m
        );
    }

    vMultiplied(v) {
        return new v3(
            this.x * v.x,
            this.y * v.y,
            this.z * v.z
        );
    }

    vDivided(v) {
        return new v3(
            this.x / v.x,
            this.y / v.y,
            this.z / v.z
        );
    }

    clamped(min, max) {
        return new v3(
            Math.min(max, Math.max(min, this.x)),
            Math.min(max, Math.max(min, this.y)),
            Math.min(max, Math.max(min, this.z))
        );
    }

    direction(p) {
        return new v3(
            p.x - this.x,
            p.y - this.y,
            p.z - this.z
        );
    }

    reverted() {
        return new v3(
            -this.x,
            -this.y,
            -this.z
        );
    }

    cross(v) {
        return new v3(
            this.y * v.z - this.z * v.y,
            this.z * v.x - this.x * v.z,
            this.x * v.y - this.y * v.x
        );
    }

    dot(v) {
        return this.x * v.x
            + this.y * v.y
            + this.z * v.z;
    }

    reflect(n) {
        let d = this.dot(n);
        n = n.multiplied(2 * d);

        return new v3(
            this.x - n.x,
            this.y - n.y,
            this.z - n.z
        );
    }

    refract(n, r) {
        let t = Math.min(this.multiplied(-1).dot(n), 1);
        let perp = this.vAdded(n.multiplied(t)).multiplied(r);
        let para = n.multiplied(-Math.sqrt(Math.abs(1 - perp.sizeSq())));

        return perp.vAdded(para);
    }

    rotatedX(r) {
        return new v3(
            this.x,
            this.y * Math.cos(r) - this.z * Math.sin(r),
            this.y * Math.sin(r) + this.z * Math.cos(r)
        );
    }

    rotatedY(r) {
        return new v3(
            this.x * Math.cos(r) + this.z * Math.sin(r),
            this.y,
            this.x * -Math.sin(r) + this.z * Math.cos(r)
        );
    }

    rotatedZ(r) {
        return new v3(
            this.x * Math.cos(r) - this.y * Math.sin(r),
            this.x * Math.sin(r) + this.y * Math.cos(r),
            this.z
        );
    }

    fromComponents(order) {
        let keys = order.split('');

        return new v3(
            this[keys[0]],
            this[keys[1]],
            this[keys[2]]
        );
    }

    getObject() {
        return {
            x: this.x,
            y: this.y,
            z: this.z
        };
    }

    static getRandom() {
        return new v3(
            -1 + Math.random() * 2,
            -1 + Math.random() * 2,
            -1 + Math.random() * 2
        );
    }

    static getRandomUnitSphere(r) {
        while (true) {
            let p = v3.getRandom();
            if (p.sizeSq() > 1) {
                continue;
            }
            return p.normalized().multiplied(r);
        }
    }

    static getRandomHemiSphere(normal) {
        let v = v3.getRandomUnitSphere(1);
        if(v.dot(normal) > 0) {
            return v;
        }
        return v.multiplied(-1);
    }
}