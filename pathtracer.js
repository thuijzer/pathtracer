'use strict';

importScripts('vmath.js', 'nishita.js');

/*
class Aabb {
    constructor(min, max) {
        this.min = min;
        this.max = max;
    }

    hit(r, minT, maxT) {
        for (let a = 0; a < 3; a++) {
            let k = ['x', 'y', 'z'][a];
            let invD = 1 / r.d[k];
            let t0 = (this.min[k] - r.o[k]) * invD;
            let t1 = (this.max[k] - r.o[k]) * invD;
            if (invD < 0) {
                let tmp = t0;
                t0 = t1;
                t1 = tmp;
            }
            minT = t0 > minT ? t0 : minT;
            maxT = t1 < maxT ? t1 : maxT;
            if (tMax <= tMin) {
                return false;
            }
        }

        return true;
    }
}
*/

// MISC ________________________________________________________________________________________________________________

class Debug {
    static log(...p) {
        console.log(...p);
        // Then stop the execution so we can log inside heavy loops without crashing the browser
        throw new Error("Debug.log");
    }
}

class PseudoRandom {
    static _numbers = [];

    static get(p) {
        if (PseudoRandom._numbers.length === 0) {
            PseudoRandom._generate();
        }

        let m = 2;
        let i = Math.round(p.x * m ^ p.y * m ^ p.z * m);
        i = Math.max(0, i % PseudoRandom._numbers.length);

        return PseudoRandom._numbers[i];
    }

    static _generate() {
        PseudoRandom._numbers = [];
        PseudoRandom._i = 0;
        let seed = 8927365;
        let nr = seed;
        let w = 0;
        for (let r = 0; r < 10000; r++) {
            nr *= nr;
            nr += (w += seed);
            nr = parseInt(nr.toString().substr(-3));
            PseudoRandom._numbers.push(nr / 1000);
            if (nr <= 0) {
                seed++;
                nr = seed;
            }
        }
    }
}

class Perlin {
    static _rx = [];
    static _ry = [];
    static _rz = [];

    static generate
}

class ColorConvertors {
    static wavelengthToRGB(wavelength, gamma) {
        if (typeof gamma === 'undefined') {
            gamma = 0.8;
        }

        let r, g, b, attenuation;

        if (wavelength >= 380 && wavelength <= 440) {
            attenuation = 0.3 + 0.7 * (wavelength - 380) / (440 - 380);
            r = Math.pow((-(wavelength - 440) / (440 - 380)) * attenuation, gamma);
            g = 0.0;
            b = Math.pow(attenuation, gamma);
        } else if (wavelength >= 440 && wavelength <= 490) {
            r = 0.0;
            g = Math.pow((wavelength - 440) / (490 - 440), gamma);
            b = 1.0;
        } else if (wavelength >= 490 && wavelength <= 510) {
            r = 0.0;
            g = 1.0;
            b = Math.pow(-(wavelength - 510) / (510 - 490), gamma);
        } else if (wavelength >= 510 && wavelength <= 580) {
            r = Math.pow((wavelength - 510) / (580 - 510), gamma);
            g = 1.0;
            b = 0.0;
        } else if (wavelength >= 580 && wavelength <= 645) {
            r = 1.0;
            g = Math.pow(-(wavelength - 645) / (645 - 580), gamma);
            b = 0.0;
        } else if (wavelength >= 645 && wavelength <= 750) {
            attenuation = 0.3 + 0.7 * (750 - wavelength) / (750 - 645);
            r = Math.pow(attenuation, gamma);
            g = 0.0;
            b = 0.0;
        } else {
            r = 0.0;
            g = 0.0;
            b = 0.0;
        }

        return new v3(r, g, b);
    }


    static hue2rgb(p, q, t) {
        if (t < 0) t += 1;
        if (t > 1) t -= 1;
        if (t < 1 / 6) return p + (q - p) * 6 * t;
        if (t < 1 / 2) return q;
        if (t < 2 / 3) return p + (q - p) * (2 / 3 - t) * 6;

        return p;
    }


    static hslToRgb(h, s, l) {
        let r, g, b;

        if (s === 0) {
            r = g = b = l; // achromatic
        } else {
            let q = l < 0.5 ? l * (1 + s) : l + s - l * s;
            let p = 2 * l - q;
            r = ColorConvertors.hue2rgb(p, q, h + 1 / 3);
            g = ColorConvertors.hue2rgb(p, q, h);
            b = ColorConvertors.hue2rgb(p, q, h - 1 / 3);
        }

        return new v3(r, g, b);
    }
}


// DTO _________________________________________________________________________________________________________________

class Scatter {
    constructor(ray, attenuation) {
        this.ray = ray;
        this.attenuation = attenuation;
    }
}

class Ray {
    // origin, direction, time
    constructor(o, d, t) {
        this.o = o;
        this.d = d;
        this.t = typeof t === 'undefined' ? .0 : t;
    }

    size() {
        return this.o.distance(this.d);
    }

    at(t) {
        return new v3(
            this.o.x + this.d.x * t,
            this.o.y + this.d.y * t,
            this.o.z + this.d.z * t
        );
    }
}


// CAMERA ______________________________________________________________________________________________________________

class Camera {
    // origin, target, zoom, focus, aperture, time0, time1
    constructor(o, t, focus, zoom, aperture, t0, t1) {
        this.o = o;
        this.zoom = zoom;
        this.aperture = aperture;
        this.forward = this.o.direction(t).normalized();
        this.right = (new v3(0, 0, 1)).cross(this.forward).normalized().multiplied(-1);
        this.up = this.forward.cross(this.right).normalized().multiplied(-1);
        this.focusDistance = this.o.distance(focus);
        this.t0 = typeof t0 === 'undefined' ? .0 : t0;
        this.t1 = typeof t1 === 'undefined' ? .0 : t1;
    }

    getUVRay(u, v) {

        let disk = v3.getRandomUnitSphere(this.aperture);
        let o = new v3(
            this.o.x + this.right.x * disk.x,
            this.o.y,
            this.o.z + this.up.z * disk.z
        );

        let focusDistance = this.focusDistance * (1 / this.zoom);

        return new Ray(
            o,
            o.direction(
                this.o
                    .vAdded(this.forward.multiplied(this.zoom).multiplied(focusDistance))
                    .vAdded(this.right.multiplied(u).multiplied(focusDistance))
                    .vAdded(this.up.multiplied(v).multiplied(focusDistance))
            ),
            this.t0 + Math.random() * (this.t1 - this.t0),
        );
    }
}


// TEXTURES ____________________________________________________________________________________________________________

class Texture {
    getColor(u, v, p) {
        return new v3();
    }
}

class SolidColorTexture extends Texture {
    constructor(color) {
        super();
        this.color = color;
    }

    getColor(hit) {
        return this.color;
    }
}

class NoiseTexture extends Texture {
    constructor(scale) {
        super();
        this.scale = scale;
    }

    getColor(hit) {
        return new v3(1).multiplied(
            PseudoRandom.get(hit.uv.vMultiplied(this.scale))
        );
    }
}

class NormalTexture extends Texture {
    getColor(hit) {
        return hit.normal.added(1).clamped(0, 1);
    }
}

class ImageTexture extends Texture {
    constructor() {
        super();

    }

    setImageData(imageData, w, h) {
        this.imageData = imageData;
        this.w = w;
        this.h = h;
    }

    getColor(hit) {
        if (
            typeof this.imageData === 'undefined'
            || typeof this.w === 'undefined'
            || typeof this.h === 'undefined'
            || this.w === 0
            || this.h === 0
        ) {
            return new v3(1, 0, 1);
        }

        let u = Math.max(0, Math.min(1, hit.uv.x));
        let v = Math.max(0, Math.min(1, hit.uv.y));

        let x = Math.round(u * (this.w - 1));
        let y = Math.round(v * (this.h - 1));
        let i = (x + y * this.w) * 4;

        return new v3(
            this.imageData[i] / 255,
            this.imageData[i + 1] / 255,
            this.imageData[i + 2] / 255
        );
    }
}

class CheckerTexture extends Texture {
    constructor(tex1, tex2, scale) {
        super();
        this.tex1 = tex1;
        this.tex2 = tex2;
        this.scale = scale;
    }

    getColor(hit) {
        let sines =
            Math.sin(this.scale.x * hit.uv.x)
            * Math.sin(this.scale.y * hit.uv.y);

        return sines < 0
            ? this.tex1.getColor(hit)
            : this.tex2.getColor(hit);
    }
}


// MATERIALS ___________________________________________________________________________________________________________

class Material {
    constructor(texture) {
        this.texture = texture;
    }

    scatter(ray, hit) {
        return new Scatter(
            new Ray(new v3(), new v3()),
            new v3()
        );
    }

    emit(hit) {
        return null;
    }
}

class LambertianMaterial extends Material {
    scatter(ray, hit) {
        let d = hit.normal.vAdded(v3.getRandom());
        if (d.sizeSq() < Number.EPSILON) {
            d = hit.normal;
        }

        return new Scatter(
            new Ray(
                hit.p,
                d,
                ray.t
            ),
            this.texture.getColor(hit)
        );
    }
}

class MetalMaterial extends Material {
    constructor(texture, roughness) {
        super(texture);
        this.roughness = roughness;
    }


    scatter(ray, hit) {
        let d = ray.d.reflect(hit.normal);

        return new Scatter(
            new Ray(
                hit.p,
                d.vAdded(v3.getRandomUnitSphere(this.roughness)),
                ray.t
            ),
            this.texture.getColor(hit)
        );
    }
}

class DielectricMaterial extends Material {
    constructor(texture, roughness, ior, dispersion) {
        super(texture);
        this.roughness = roughness;
        this.ior = ior;
        this.dispersion = typeof dispersion === 'undefined' ? 0 : dispersion;
    }

    static reflectance(cos, ratio) {
        let r0 = (1 - ratio) / (1 + ratio);
        r0 = r0 * r0;

        return r0 + (1 - r0) * Math.pow(1 - cos, 5);
    }

    scatter(ray, hit) {
        // ior per color to simulate dispersion
        // R = 0, G = .5, B = 1
        let f = Math.random();
        let rgb = ColorConvertors.hslToRgb(f, 1, .5);
        let ior = this.ior - this.dispersion * .5 + this.dispersion * f;

        let ratio = hit.frontFacing ? 1 / ior : ior;
        let cos = Math.min(ray.d.multiplied(-1).dot(hit.normal), 1);
        let sin = Math.sqrt(1 - cos * cos);
        let reflect = ratio * sin > 1;
        let d = reflect || DielectricMaterial.reflectance(cos, ratio) > Math.random()
            ? ray.d.normalized().reflect(hit.normal)
            : ray.d.normalized().refract(hit.normal, ratio);

        return new Scatter(
            new Ray(
                hit.p,
                d.vAdded(v3.getRandomUnitSphere(this.roughness)),
                ray.t
            ),
            this.texture.getColor(hit).vMultiplied(rgb).multiplied(2)
        );
    }
}

class EmissiveMaterial extends Material {
    constructor(texture, power) {
        super(texture);
        this.power = power;
    }

    scatter(ray, hit) {
        return null;
    }

    emit(hit) {
        return this.texture.getColor(hit).multiplied(this.power);
    }
}


// HITTABLES ___________________________________________________________________________________________________________

class Hit {
    constructor(t, p, normal, frontFacing, material, uv) {
        this.t = t;
        this.p = p;
        this.normal = normal;
        this.frontFacing = frontFacing;
        this.material = material;
        this.uv = uv;
    }
}

class Hittable {
    constructor(material) {
        this.material = material;
    }

    getHit(r, tMin, tMax) {
        return null;
    }
}

class Sphere extends Hittable {
    // radius, material, center0, time0, center1, time1
    constructor(radius, material, c0, t0, c1, t1) {
        super(material);
        this.radius = radius;
        this.c0 = c0;
        this.t0 = typeof t0 === 'undefined' ? .0 : t0;
        this.c1 = typeof c1 === 'undefined' ? this.c0 : c1;
        this.t1 = typeof t1 === 'undefined' ? this.t0 : t1;
    }

    getCenter(t) {
        if (this.t1 === 0) {
            return this.c0;
        }

        return this.c0.vAdded(this.c1.vSubstracted(this.c0).multiplied((t - this.t0) / (this.t1 - this.t0)));
    }

    getUV(normal) {
        let phi = Math.atan2(-normal.x, -normal.y) + Math.PI;
        let theta = Math.acos(-normal.z);

        return new v3(
            1 - phi / (2 * Math.PI),
            1 - theta / Math.PI,
            0
        );
    }

    getHit(r, tMin, tMax) {
        let oc = this.getCenter(r.t).direction(r.o);
        let a = r.d.sizeSq();
        let b = oc.dot(r.d);
        let c = oc.sizeSq() - Math.pow(this.radius, 2);
        let d = Math.pow(b, 2) - a * c;
        if (d < 0) {
            return null;
        }

        let sqrtD = Math.sqrt(d);
        let root = (-b - sqrtD) / a;

        if (root < tMin || tMax < root) {
            root = (-b + sqrtD) / a;
            if (root < tMin || tMax < root) {
                return null;
            }
        }

        let p = r.at(root);

        let outNormal = this.getCenter(r.t).direction(p).normalized();
        let frontFacing = r.d.dot(outNormal) < 0;

        let uv = this.getUV(outNormal);

        return new Hit(
            root,
            p,
            frontFacing ? outNormal : outNormal.reverted(),
            frontFacing,
            this.material,
            uv
        );
    }
}

class World {
    constructor() {
        this.hittables = [];
    }

    getHit(r, minT, maxT) {
        let minHit = null;
        let minD = Number.MAX_VALUE;
        for (let i = 0; i < this.hittables.length; i++) {
            let hit = this.hittables[i].getHit(r, minT, maxT);
            if (hit !== null && hit.t < minD) {
                minD = hit.t;
                minHit = hit;
            }
        }

        return minHit;
    }
}


// SETUP _______________________________________________________________________________________________________________

let samples = 30;
let bounces = 30;
let frame = 0;
let minT = 0.00001;
let maxT = Number.MAX_VALUE;
let atmosphere = new Atmosphere(
    0,
        1,
        1,
    .5
);

let checkerTexture = new CheckerTexture(
    new SolidColorTexture(new v3(.8, .8, .8)),
    new SolidColorTexture(new v3(.2, .2, .2)),
    new v3(50)
);

let earthTexture = new ImageTexture();
let uvTexture = new ImageTexture();
let environmentTexture = new ImageTexture();

let materials = {
    checker: new LambertianMaterial(checkerTexture),
    earth: new LambertianMaterial(earthTexture),
    uv: new LambertianMaterial(uvTexture),
    white: new LambertianMaterial(new SolidColorTexture(new v3(.8, .8, .8))),
    orange: new LambertianMaterial(new SolidColorTexture(new v3(.9, .5, .2))),
    red: new MetalMaterial(new SolidColorTexture(new v3(.8, .0, .0)), 0.1),
    green: new LambertianMaterial(new SolidColorTexture(new v3(.0, .8, .0))),
    metal: new MetalMaterial(new SolidColorTexture(new v3(.9, .9, .9)), .01),
    gold: new MetalMaterial(new SolidColorTexture(new v3(.9, .6, .1)), .001),
    glass: new DielectricMaterial(new SolidColorTexture(new v3(.98, 1, .98)), 0, 1.5, 0.01),
    bubble: new DielectricMaterial(new SolidColorTexture(new v3(1, 1, 1)), 0, 1.000001, 0.1),
    environment: new EmissiveMaterial(environmentTexture, 1),
    normal: new LambertianMaterial(new NormalTexture()),
    noise: new LambertianMaterial(new NoiseTexture(new v3(10))),
    light: new EmissiveMaterial(new SolidColorTexture(new v3(1, .8, .6)), 10),
};


let world = new World();
world.hittables = [
    new Sphere(.5, materials.metal, new v3(-.55, 0, .5)),
    new Sphere(.5, materials.checker, new v3(.55, 0, .5)),
    new Sphere(.2, materials.glass, new v3(-.3, -1, .2)),
    //new Sphere(.1, materials.light, new v3(.3, -.8, .1)),
    new Sphere(10000, materials.earth, new v3(0, 0, -10000)),
    //new Sphere(10000, materials.environment, new v3(0, 0, 0)),
];
let o = .6;
for (let i = 0; i < Math.PI * 2; i += .5) {
    let r = 1.3;
    let c = new v3(r * Math.sin(i + o), r * Math.cos(i + o), .7 + .3 * Math.sin(i * 5) + frame * 0.01);
    world.hittables.push(
        new Sphere(.1, materials.bubble, c),
    );
}

let cameraOrigin = new v3(1, -2.3, 0.4);
let camera = new Camera(
    cameraOrigin,
    new v3(0, 0, .7),
    new v3(0, 0, .5),
    1,
    .01,
    0,
    1
);


// RENDERER ____________________________________________________________________________________________________________

let getColor = function (r, bounce) {
    if (typeof bounce === 'undefined') {
        bounce = 0;
    }
    if (bounce >= bounces) {
        return new v3();
    }

    let hit = world.getHit(r, minT, maxT);

    if (hit === null) {
        return atmosphere.getIncidentLight(r, minT, maxT);
    }

    let scatter = hit.material.scatter(r, hit);
    let emitted = hit.material.emit(hit);
    if (scatter === null) {
        return emitted === null ? new v3() : emitted;
    }

    return getColor(scatter.ray, bounce + 1)
        .vMultiplied(scatter.attenuation);
}

self.onmessage = function (e) {
    if (e.data.action === 'setData') {
        samples = e.data.samples;
        bounces = e.data.bounces;
        frame = e.data.frame;
        earthTexture.setImageData(
            e.data.textures.earthTexture.data,
            e.data.textures.earthTexture.width,
            e.data.textures.earthTexture.height
        );
        uvTexture.setImageData(
            e.data.textures.uvTexture.data,
            e.data.textures.uvTexture.width,
            e.data.textures.uvTexture.height
        );
        environmentTexture.setImageData(
            e.data.textures.environmentTexture.data,
            e.data.textures.environmentTexture.width,
            e.data.textures.environmentTexture.height
        );
        atmosphere = new Atmosphere(
            e.data.sunRotation,
            e.data.sunElevation,
            1,
            1
        );
        return;
    }

    let result = {
        done: false,
        pixels: []
    };

    let xMax = Math.min(e.data.x + e.data.bucketSize, e.data.w);
    let yMax = Math.min(e.data.y + e.data.bucketSize, e.data.h);

    // Display the bucket
    let s = 10;
    let t = 1;
    for (let y = e.data.y; y < yMax; y++) {
        for (let x = e.data.x; x < xMax; x++) {
            if (x * y % 2 === 0) {
                continue;
            }
            result.pixels.push(
                {
                    x: x,
                    y: y,
                    color: (new v3(.5)).getObject(),
                }
            );
        }
    }
    self.postMessage(result);

    // Start rendering
    result.pixels = [];

    let u = 0;
    let um = e.data.bucketSize * e.data.bucketSize * .25;
    for (let y = e.data.y; y < yMax; y++) {
        for (let x = e.data.x; x < xMax; x++) {
            let color = new v3();
            for (let s = 0; s < samples; s++) {
                let xs = x + Math.random();
                let ys = y + Math.random();
                let u = ((xs / e.data.w) - .5) * (e.data.w / e.data.h);
                let v = -((ys / e.data.h) - .5);
                let camRay = camera.getUVRay(u, v);
                color = color.vAdded(getColor(camRay));
            }
            result.pixels.push(
                {
                    x: x,
                    y: y,
                    color: color.multiplied(1 / samples).getObject(),
                }
            );

            u++;
            if (u >= um) {
                u = 0;
                self.postMessage(result);
            }
        }
    }
    result.done = true;
    self.postMessage(result);
};